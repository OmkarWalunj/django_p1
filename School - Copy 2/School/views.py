from django.http import HttpResponse
from django.shortcuts import render
from recruitmentdata.models import data
from Admissiondata.models import Adata
def homePage(request):
    return render(request,"practice.html")

def recruitment(request):
    return render(request,"recruitment.html")

def admission(request):
    return render(request,"index.html")

def nda(request):
    return render(request,"nda.html")

def mess(request):
    return render(request,"mess.html")

def stats(request):
    return render(request,"stats.html")

def recruitment__data(request):

    if request.method=="POST":
        name=request.POST.get("name")
        email=request.POST.get("email")
        mobile=request.POST.get("mobile")
        address=request.POST.get("address")
        class1=request.POST.get("class")
        course=request.POST.get("course")
        text=request.POST.get("text")

        em=data(Name=name,Email=email,Mobile=mobile,Address=address,Class1=class1,Course=course,Text=text)
        em.save()
        print(name,email,mobile,address,class1,course,text)
    
    return render(request,"recruitment.html")

def admission__data(request):
    
    if request.method=="POST":
        name=request.POST.get("name")
        mobile=request.POST.get("mobile")
        birthdate=request.POST.get("DOB")
        gender=request.POST.get("mf")
        address=request.POST.get("address")
        city=request.POST.get("city")
        taluka=request.POST.get("Taluka")
        district=request.POST.get("district")
        state=request.POST.get("state")
        country=request.POST.get("country")
        pincode=request.POST.get("Pincode")
        faculty=request.POST.get("faculty")
        program=request.POST.get("program")
        course =request.POST.get("course")
        subject =request.POST.get("chkbx")
        fathername =request.POST.get("Fathername")
        occupation =request.POST.get("Occupation")
        fAddress =request.POST.get("Address")
        contactNo =request.POST.get("ContactNo")
        mothersName =request.POST.get("Mother'sName")
        motherOccupation=request.POST.get("MotherOccupation")
        motherAddress =request.POST.get("MotherAddress")
        motherContactNo =request.POST.get("MotherContactNo")
        guardainName =request.POST.get("Guardain's Name")
        guardainOccupation =request.POST.get("Guardain'sOccupation")
        guardainAddress =request.POST.get("Guardain'sAddress")
        guardainContactNo =request.POST.get("Guardain'sContact No")
        relation =request.POST.get("Relation")
        schoolName =request.POST.get("School Name")
        passedYear =request.POST.get("Passed Year")
        percentage =request.POST.get("Percentage")

        data=Adata(Name=name,Mobile=mobile,Address=address,Gender=gender,DOB=birthdate,City=city,Taluka=taluka,District=district,State=state,Country=country,
                Pincode=pincode,Faculty=faculty,Program=program,Subject=subject,FarherName=fathername,Faddress=fAddress,Occupation=occupation,Contact=contactNo,MotherName=mothersName,
                MotherOccupation=motherOccupation,MAddress=motherAddress,Mcontact=motherContactNo,Gaddress=guardainAddress, 
                GName=guardainName,Goccupation=guardainOccupation,Gcontact=guardainContactNo,Relation=relation ,School=schoolName,Passedyear= passedYear,Percentage=percentage,Course=course)
        data.save()
        print(name,mobile,address,course)
    return render(request,"index.html")

def save_form(request):

    return HttpResponse("Hello world")