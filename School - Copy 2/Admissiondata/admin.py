from django.contrib import admin

from Admissiondata.models import Adata

class Admissiondata_data(admin.ModelAdmin):
    list_display=('Name','Mobile','DOB','Gender','Address','City','Taluka','District','State','Country','Pincode','Faculty','Program','Course','Subject','FarherName','Occupation','Faddress','Contact','MotherName','MotherOccupation','MAddress','Mcontact','GName','Goccupation','Gaddress','Gcontact','Relation','School','Passedyear','Percentage')

admin.site.register(Adata,Admissiondata_data)
# Register your models here.
