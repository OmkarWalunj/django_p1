"""School URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from School import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.homePage),
    path('templates/practice.html/', views.homePage),
    path('templates/recruitment.html/', views.recruitment),
    path('templates/index.html/', views.admission),
    path('templates/nda.html/', views.nda),
    path('templates/mess.html/', views.mess),
    path('templates/stats.html/', views.stats),
    path('saveform/', views.recruitment__data ,name="saveform"),
    path('Asaveform/', views.admission__data ,name="Asaveform"),
]

