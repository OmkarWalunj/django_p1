from django.apps import AppConfig


class RecruitmentdataConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'recruitmentdata'
