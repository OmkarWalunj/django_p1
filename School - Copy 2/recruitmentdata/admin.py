from django.contrib import admin

from recruitmentdata.models import data

class recruitment_data(admin.ModelAdmin):
    list_display=('Name','Email','Mobile','Address','Class1','Course','Text')

admin.site.register(data,recruitment_data)

# Register your models here.
