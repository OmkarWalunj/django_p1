from django.apps import AppConfig


class AdmissiondataConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Admissiondata'
